import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MoviePartsComponent } from './movieParts/movieParts.component';
import { MoviePartsEditComponent } from './movieParts/moviePartsEdit/moviePartsEdit.component';
import { MoviesComponent } from './movies/movies.component';
import { MoviesDetailsComponent } from './movies/moviesDetails/moviesDetails.component';
import { MoviesEditComponent } from './movies/moviesEdit/moviesEdit.component';
import { NavbarComponent } from './navbar/navbar.component';

const appRoutes: Routes = [
  { path: "", component: MoviesComponent },
  { path: "addMovies", component: MoviesEditComponent },
  { path: "details", component: MoviesDetailsComponent },
  { path: "addMoviePart", component: MoviePartsEditComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    MoviesComponent,
    NavbarComponent,
    MoviePartsComponent,
    MoviePartsEditComponent,
    MoviesEditComponent,
    MoviesDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  exports: [

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}