import { Component } from "@angular/core";
import { MovieParts } from "./movieParts.model";

@Component({
    selector:"app-movieParts",
    templateUrl:'./movieParts.component.html',
    styleUrls: ['./movieParts.component.css']
})

export class MoviePartsComponent{

    moviePartAdd : boolean;

    constructor(){
        this.moviePartAdd=false;
    }

    moviePartAdded() {
        this.moviePartAdd = !this.moviePartAdd;
    }

    movieParts: MovieParts[] = [
        new MovieParts("part1","https://logos.flamingtext.com/Word-Logos/part-design-china-name.png",1,222,33),
        new MovieParts("part2","https://logos.flamingtext.com/Word-Logos/part-design-china-name.png",2,111,34),
        new MovieParts("part3","https://logos.flamingtext.com/Word-Logos/part-design-china-name.png",3,23,6),
        new MovieParts("part4","https://logos.flamingtext.com/Word-Logos/part-design-china-name.png",4,23,6),

    ]

    onMoviePartAdded(moviePartData: { name: string, partNumber: number, likeRate: number, dislikeRate: number }) {
        // alert("here");
        this.movieParts.push({
          name: moviePartData.name,
          partNumber: moviePartData.partNumber,
          imagePath: "https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Test-Logo.svg/783px-Test-Logo.svg.png",
          likeRate: moviePartData.likeRate,
          dislikeRate: moviePartData.dislikeRate     
        });
    }

}