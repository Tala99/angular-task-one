export class MovieParts{
    public name : string;
    public imagePath : string;
    public partNumber : number;
    public likeRate : number;
    public dislikeRate : number;

    constructor(name: string, imagePath: string,  partNumber: number, likeRate: number, dislikeRate: number){
        this.name = name;
        this.imagePath = imagePath;
        this.partNumber = partNumber;
        this.likeRate = likeRate;
        this.dislikeRate = dislikeRate;
    }
}