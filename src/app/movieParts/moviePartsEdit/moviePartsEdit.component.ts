import { Component, Output } from "@angular/core";
import { EventEmitter } from "@angular/core";

@Component({
    selector: "app-moviePartsEdit",
    templateUrl: ".//moviePartsEdit.component.html"
})
export class MoviePartsEditComponent {
    name : string;
    partNumber : number;
    likeRate : number;
    dislikeRate : number;

    constructor() {
        this.name = ' ';
        this.partNumber = 0;
        this.likeRate = 0;
        this.dislikeRate = 0;
    }

    @Output() moviePartAdded = new EventEmitter<{name : string, partNumber : number, likeRate : number, dislikeRate : number}>();

    onMoviePartAdded(event : any) {
        this.moviePartAdded.emit({name:this.name, partNumber:this.partNumber, likeRate:this.likeRate, dislikeRate:this.dislikeRate});
    }

}