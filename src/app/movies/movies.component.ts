import { Component, Input, OnInit } from '@angular/core';
import { Movies } from './movies.model';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {

  constructor() { }

  addMovie : boolean = false;

  canAddMovie() {
    this.addMovie = !this.addMovie;
  }

  // @Input()
  // movie!: { name: string; imagePath: string; likeRate: number; dislikeRate: number; type: string; };

  movies: Movies[] = [
    new Movies("A Test Movie1", "https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Test-Logo.svg/783px-Test-Logo.svg.png", 300, 20, "action"),
    new Movies("A Test Movie2", "https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Test-Logo.svg/783px-Test-Logo.svg.png", 500, 20, "comedy"),
    new Movies("A Test Movie3", "https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Test-Logo.svg/783px-Test-Logo.svg.png", 323, 10, "comedy"),
    new Movies("A Test Movie4", "https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Test-Logo.svg/783px-Test-Logo.svg.png", 100, 20, "action")

  ];

  onMovieAdded(movieData: { name: string, likeRate: number, dislikeRate: number, type: string }) {
    // alert("here");
    this.movies.push({
      name: movieData.name,
      imagePath: "https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Test-Logo.svg/783px-Test-Logo.svg.png",
      likeRate: movieData.likeRate,
      dislikeRate: movieData.dislikeRate,
      type: movieData.type
    })
    console.log(JSON.stringify({
      name: movieData.name,
      imagePath: "https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Test-Logo.svg/783px-Test-Logo.svg.png",
      likeRate: movieData.likeRate,
      dislikeRate: movieData.dislikeRate,
      type: movieData.type
    }));
  }

  ngOnInit(): void {
  }

}
