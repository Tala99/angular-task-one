export class Movies {
    public name : string;
    public imagePath : string;
    public likeRate : number; 
    public dislikeRate : number;
    public type : string;

    constructor(name: string, imagePath: string, likeRate: number, dislikeRate: number, type: string) {
        this.name = name;
        this.imagePath = imagePath;
        this.likeRate = likeRate;
        this.dislikeRate = dislikeRate;
        this.type = type;
    }
}