import { Component } from "@angular/core";
import { Movies } from "../movies.model";

@Component({
    selector:"app-moviesDetails",
    templateUrl:"./moviesDetails.component.html"
})
export class MoviesDetailsComponent{

    movies : Movies[] = [
        new Movies("A Test Movie1", "https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Test-Logo.svg/783px-Test-Logo.svg.png",300,20,"action")
    ]
    
    
}