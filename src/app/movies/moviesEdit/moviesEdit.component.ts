import { Component, Output } from "@angular/core";
import { EventEmitter } from "@angular/core";

@Component({
    selector: 'app-moviesEdit',
    templateUrl: './/moviesEdit.component.html'
})
export class MoviesEditComponent {

    name :string;
    likeRate :number;
    dislikeRate :number;
    type :string;

    constructor() {
        this.name=' ';
        this.likeRate=0;
        this.dislikeRate=0;
        this.type=' ';
    }

    @Output() movieAdded = new EventEmitter<{ name: string, likeRate: number, dislikeRate: number, type: string }>();

    onMovieAdded(event: any) {        
        //console.log(JSON.stringify( {name: this.name, likeRate: this.likeRate, dislikeRate: this.dislikeRate, type: this.type}));
        this.movieAdded.emit({ name: this.name, likeRate: this.likeRate, dislikeRate: this.dislikeRate, type: this.type });
    }

}